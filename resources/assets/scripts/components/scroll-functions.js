export let scrollFunctions = {
    init() {
        window.addEventListener('load', () => this.build(), false);
    },
    build() {
        let stickyHeader = document.querySelector('.c-header');
        $(window).scroll(function () {
            let st = $(this).scrollTop();
            if (st > 20) {
                stickyHeader.classList.add("c-header_sticky");
            } else {
                stickyHeader.classList.remove("c-header_sticky");
            }
        });
    },
};
