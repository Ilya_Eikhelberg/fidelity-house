let isOpen = false,
menuTrigger = $('[data-trigger="nav"]');

function events(){

    $(document).on('keyup', function (e) {
        if (e.keyCode === 27 && isOpen) {
            toggle();
        }
    });

    $(menuTrigger).on('click', function () {
        toggle();
    });

}
function toggle() {
    isOpen = !isOpen;
    $('body').toggleClass('js-nav-open');
}

export default {
    init() {
        events();
    },
}
