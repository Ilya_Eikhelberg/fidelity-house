import 'waypoints/lib/noframework.waypoints';
import 'waypoints/lib/shortcuts/inview';

export default function () {
    let container = document.querySelectorAll('.home-slider');
    let scrollTrigger = document.querySelector('.c-scroll-trigger');
    let homeScrollSectionSecond = document.querySelectorAll('.l-home-slider__content')[1];

    if (container.length) {
        new HomeSlider(container[0]);

        scrollTrigger.addEventListener('click', () => {
            homeScrollSectionSecond.scrollIntoView({ behavior: 'smooth' });
        });
    }
}

class HomeSlider {
    constructor(container) {
        this.container = container;
        this.loading = true;
        this.slideNodes = document.querySelectorAll('[data-slide-id]');
        setTimeout(() => {
            this.makeSvg();
            this.makeWaypoint();
            this.loading = false;
        }, 1000);
    }

    set loading(state) {
        if (state) {
            this.container.classList.add('loading');
        }
        else this.container.classList.remove('loading');
    }

    makeSvg() {
        let mask = document.querySelector('.c-svg-mask');
        let slides = {};

        for (let i = 0; i < this.slideNodes.length; ++i) {
            let node = this.slideNodes[i].dataset;
            slides[node.slideId] = node.slideUrl;
        }
        mask.innerHTML = this.mask(slides);
    }

    makeWaypoint() {
        let homeScrollSlide = document.querySelectorAll('[data-slide-id]');
        let waypoints = window.wp = [];

        for (let i = 0; i < homeScrollSlide.length; ++i) {
            let wayp = new window.Waypoint.Inview({
                element: homeScrollSlide[i],
                enter: function () {
                    let slideId = homeScrollSlide[i].dataset.slideId;
                    $(`[data-home-slide=${slideId}]`).addClass('active').siblings().removeClass('active');
                },
            });
            waypoints.push(wayp);
        }
        $('[data-home-slide="1"]').addClass('active').siblings().removeClass('active');
    }

    mask(slides) {
        let imagesGroup = '';
        Object.keys(slides).forEach(id => {
            imagesGroup += `<image mask="url(#myMask)"
                       style="width: 100%"
                       data-home-slide='${id}'
                       xlink:href='${slides[id]}'></image>`;
        });

        return `<svg viewBox="0 0 215 267">
                <defs>
                    <linearGradient id="linearGradient32"
                                    spreadMethod="pad"
                                    gradientTransform="matrix(80.608124,-139.61737,-139.61737,-80.608124,18.876877,177.85751)"
                                    gradientUnits="userSpaceOnUse"
                                    y2="0" x2="1" y1="0" x1="0">
                        <stop id="stop24" offset="0" style="stop-opacity:1;stop-color:#00a7b5"></stop>
                        <stop id="stop26" offset="0.00102163" style="stop-opacity:1;stop-color:#00a7b5"></stop>
                        <stop id="stop28" offset="0.29723152" style="stop-opacity:1;stop-color:#00a7b5"></stop>
                        <stop id="stop30" offset="1" style="stop-opacity:1;stop-color:#a0dab3"></stop>
                    </linearGradient>
                </defs>
                <mask id="myMask">
                    <g transform="matrix(1.3333333,0,0,-1.3333333,0,267.40933)" id="g10">
                        <path id="path34"
                              style="fill:#ffffff;"
                              d="M 79.984,200.557 C 63.342,200.5 46.727,195.451 32.792,185.441 v 0 C 6.582,166.614 0,136.309 0,105.935 v 0 V 31.705 0 c 0.068,0.02 0.164,0.049 0.288,0.087 v 0 c 6.206,1.881 82.175,25.218 124.247,53.019 v 0 c 10.195,6.737 18.407,13.733 23.145,20.753 v 0 c 9.387,13.905 13.901,29.672 13.932,45.276 v 0 0.307 c -0.048,26.069 -12.602,51.656 -35.835,67.341 v 0 c -13.534,9.137 -29.376,13.718 -45.22,13.774 v 0 z M 71.15,107.453 c -9.635,6.504 -12.172,19.587 -5.668,29.223 v 0 c 6.504,9.634 19.587,12.172 29.222,5.668 v 0 c 9.635,-6.504 12.173,-19.588 5.668,-29.223 v 0 c -4.066,-6.023 -10.705,-9.273 -17.464,-9.273 v 0 c -4.053,-0.001 -8.147,1.167 -11.758,3.605">
                        </path>
                    </g>
                </mask>
                <rect x="0" y="0"
                      width="300"
                      height="1000"
                      mask="url(#myMask)"
                      style="fill:url(#linearGradient32);" ></rect>
               ${imagesGroup} 
            </svg>`
    }
}