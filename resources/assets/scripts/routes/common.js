import navigation from '../components/navigation';
import {scrollFunctions} from "../components/scroll-functions";
import homeSlider from '../components/home-slider';

export default {
  init() {
    // JavaScript to be fired on all pages
      navigation.init();
      scrollFunctions.init();
      homeSlider();
  },
  finalize() {
    // JavaScript to be fired on all pages, after page specific JS is fired
  },
};
