<header class="c-header">
    <a class="c-header__logo" href="{{get_home_url()}}">
        <div class="c-header__logo-main"></div>
        <div class="c-header__logo-nav"></div>
    </a>
    <div class="c-header__links c-header__links_row">
        <div class="c-menu">
            {!! wp_nav_menu(['container' => false, 'menu' => 'Main Menu', 'menu_class' => 'nav']) !!}
        </div>
        <div class="c-header__buttons-section">
            <a class="btn btn_white btn_shadow" href="#">Login</a>
        </div>
    </div>
    <div class="c-hamburger">
        <div class="c-hamburger__icon" data-trigger="nav">
            <div class="c-hamburger__bar"></div>
            <div class="c-hamburger__bar"></div>
            <div class="c-hamburger__bar"></div>
        </div>
    </div>
    <div class="c-header__popup">
        <div class="c-header__links">
            <div class="c-menu">
                {!! wp_nav_menu([ 'container' => false, 'menu' => 'Main Menu', 'menu_class' => 'menu']) !!}
            </div>
        </div>
        <div class="c-header__buttons-section">
            <div><a class="btn btn_fix-width" href="#">Login</a></div>
            <div class="m-t-20"><a class="btn btn_fix-width" href="#">Join</a></div>
        </div>
    </div>
</header>
