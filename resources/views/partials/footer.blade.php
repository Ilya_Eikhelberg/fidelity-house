@php
  $copyright = get_field('copyright', 'option');
@endphp
<footer class="c-footer">
  <div class="c-footer__top-panel">
    <div class="c-footer__logo"></div>
    <div class="c-menu">
      {!! wp_nav_menu([ 'container' => false, 'menu' => 'Footer Menu', 'menu_class' => 'nav-footer']) !!}
    </div>
  </div>
  <div class="c-footer__social-links">
    <a href="#"></a>
    <a href="#"></a>
    <a href="#"></a>
    <a href="#"></a>
  </div>
  <div class="c-footer__bottom-panel">
    <div class="c-footer__other-links">
      <a href="#">Terms & Conditions </a>
      <a href="#">Privacy Policy</a>
    </div>
    <p>© 2019 RespiteEase. All rights reserved.</p>
  </div>
</footer>
