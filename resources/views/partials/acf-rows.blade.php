@if(have_rows('flexible_content'))
    @while (have_rows('flexible_content')) @php(the_row())
    @includeIf( 'acf.'.get_row_layout() )
    @endwhile
@endif
