{{--
Template Name: About
--}}
@extends('layouts.app')

@section('content')
    <section class="l-section l-section_bg_gray">
        <div class="l-container">

            <div class="u-flex">
                <div class="l-container__side">
                    <article class="c-article">
                        <div class="h">About us</div>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Natus tempora totam temporibus cupiditate molestias adipisci voluptatibus dicta ipsa, repellendus ullam. Sit natus quaerat, tempora blanditiis similique ipsum debitis libero sed magnam ut animi molestiae et porro earum eos inventore non.</p>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Harum numquam dolore a dolor esse nihil alias qui amet, maiores, cumque molestiae ad ducimus! Perspiciatis excepturi accusantium molestias, laboriosam blanditiis adipisci?</p>
                    </article>
                </div>
                
                <div class="l-container__side">
                    <div class="c-hero">
                        <svg class="c-hero__marker" viewBox="0 0 215 267">
                            <defs>
                                <linearGradient id="linearGradient32"
                                                spreadMethod="pad"
                                                gradientTransform="matrix(80.608124,-139.61737,-139.61737,-80.608124,18.876877,177.85751)"
                                                gradientUnits="userSpaceOnUse"
                                                y2="0" x2="1" y1="0" x1="0">
                                    <stop id="stop24" offset="0" style="stop-opacity:1;stop-color:#00a7b5"></stop>
                                    <stop id="stop26" offset="0.00102163" style="stop-opacity:1;stop-color:#00a7b5"></stop>
                                    <stop id="stop28" offset="0.29723152" style="stop-opacity:1;stop-color:#00a7b5"></stop>
                                    <stop id="stop30" offset="1" style="stop-opacity:1;stop-color:#a0dab3"></stop>
                                </linearGradient>
                            </defs>
                    
                            <mask id="myMask">
                                <g transform="matrix(1.3333333,0,0,-1.3333333,0,267.40933)" id="g10">
                                    <path id="path34"
                                          style="fill:#ffffff;"
                                          d="M 79.984,200.557 C 63.342,200.5 46.727,195.451 32.792,185.441 v 0 C 6.582,166.614 0,136.309 0,105.935 v 0 V 31.705 0 c 0.068,0.02 0.164,0.049 0.288,0.087 v 0 c 6.206,1.881 82.175,25.218 124.247,53.019 v 0 c 10.195,6.737 18.407,13.733 23.145,20.753 v 0 c 9.387,13.905 13.901,29.672 13.932,45.276 v 0 0.307 c -0.048,26.069 -12.602,51.656 -35.835,67.341 v 0 c -13.534,9.137 -29.376,13.718 -45.22,13.774 v 0 z M 71.15,107.453 c -9.635,6.504 -12.172,19.587 -5.668,29.223 v 0 c 6.504,9.634 19.587,12.172 29.222,5.668 v 0 c 9.635,-6.504 12.173,-19.588 5.668,-29.223 v 0 c -4.066,-6.023 -10.705,-9.273 -17.464,-9.273 v 0 c -4.053,-0.001 -8.147,1.167 -11.758,3.605">
                                    </path>
                                </g>
                            </mask>
                    
                            <rect x="0" y="0"
                                  width="300"
                                  height="1000"
                                  mask="url(#myMask)"
                                  style="fill:url(#linearGradient32);" ></rect>
                            {{-- <image mask="url(#myMask)"
                                   style="width: 100%"
                                   xlink:href="http://img-fotki.yandex.ru/get/5607/5091629.6b/0_612e6_b9039c0d_M.jpg"></image> --}}
                        </svg>
                    
                        <figure class="c-hero__photo">
                            <img src="{{get_template_directory_uri() . '/assets/images/hero-photo.jpg'}}" alt="">
                        </figure>        
                    </div>
                </div>
            </div>

        </div>
    </section>

    <section class="l-section l-section_bg_gray_bottom">
        <div class="l-container">
            <div class="c-article__h h">Our Team</div>
            
            <div class="l-4-col">
                <div class="l-4-col__item">
                    <div class="c-team-member">
                        <figure class="c-team-member__photo">
                            <img src="{{get_template_directory_uri() . '/assets/images/team-member-photo.jpg'}}" alt="">
                        </figure>
                        <div class="c-team-member__name">Jane Doe</div>
                        <div class="c-team-member__spec">Founder</div>
                    </div>
                </div>
                <div class="l-4-col__item">
                    <div class="c-team-member">
                        <figure class="c-team-member__photo">
                            <img src="{{get_template_directory_uri() . '/assets/images/team-member-photo.jpg'}}" alt="">
                        </figure>
                        <div class="c-team-member__name">Jane Doe</div>
                        <div class="c-team-member__spec">Founder</div>
                    </div>
                </div>
                <div class="l-4-col__item">
                    <div class="c-team-member">
                        <figure class="c-team-member__photo">
                            <img src="{{get_template_directory_uri() . '/assets/images/team-member-photo.jpg'}}" alt="">
                        </figure>
                        <div class="c-team-member__name">Jane Doe</div>
                        <div class="c-team-member__spec">Founder</div>
                    </div>
                </div>
                <div class="l-4-col__item">
                    <div class="c-team-member">
                        <figure class="c-team-member__photo">
                            <img src="{{get_template_directory_uri() . '/assets/images/team-member-photo.jpg'}}" alt="">
                        </figure>
                        <div class="c-team-member__name">Jane Doe</div>
                        <div class="c-team-member__spec">Founder</div>
                    </div>
                </div>
                <div class="l-4-col__item">
                    <div class="c-team-member">
                        <figure class="c-team-member__photo">
                            <img src="{{get_template_directory_uri() . '/assets/images/team-member-photo.jpg'}}" alt="">
                        </figure>
                        <div class="c-team-member__name">Jane Doe</div>
                        <div class="c-team-member__spec">Founder</div>
                    </div>
                </div>
            </div>

        </div>
    </section>
@endsection
