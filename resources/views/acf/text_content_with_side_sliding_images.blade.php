<div class="l-home-slider home-slider">
    <div class="l-home-slider__content">
        <div class="c-intro-text"
             data-slide-id="1"
             data-slide-url="http://img-fotki.yandex.ru/get/5607/5091629.6b/0_612e6_b9039c0d_M.jpg">
            <h2 class="c-intro-text__title">The Heart Behind Homecare</h2>
            <p class="c-intro-text__article">Lorem ipsum dolor sit amet consectetur adipiscing elit, sed do eius
                mod at tempor incididunt ut labore.</p>
            <a class="btn btn" href="#">Find a Caregiver</a>
            <a class="btn btn_white" href="#">Become a Caregiver</a>
        </div>
        <button class="c-scroll-trigger">
            <svg xmlns:dc="http://purl.org/dc/elements/1.1/"
                 xmlns:cc="http://creativecommons.org/ns#"
                 xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
                 xmlns:svg="http://www.w3.org/2000/svg"
                 xmlns="http://www.w3.org/2000/svg"
                 viewBox="0 0 131.0092 131.01559"
                 height="131.01559"
                 width="131.0092"
                 xml:space="preserve"
                 id="svg2"
                 version="1.1"><metadata
                        id="metadata8"><rdf:RDF><cc:Work
                                rdf:about=""><dc:format>image/svg+xml</dc:format><dc:type
                                    rdf:resource="http://purl.org/dc/dcmitype/StillImage" /></cc:Work></rdf:RDF></metadata><defs
                        id="defs6"><clipPath
                            id="clipPath18"
                            clipPathUnits="userSpaceOnUse"><path
                                id="path16"
                                d="M 0,98.262 H 98.257 V 0 H 0 Z" /></clipPath></defs><g
                        transform="matrix(1.3333333,0,0,-1.3333333,0,131.0156)"
                        id="g10"><g
                            id="g12"><g
                                clip-path="url(#clipPath18)"
                                id="g14"><g
                                    transform="translate(49.1309,1)"
                                    id="g20"><path
                                        id="path22"
                                        style="fill:none;stroke-width:2;stroke-linecap:butt;stroke-linejoin:miter;stroke-miterlimit:10;stroke-dasharray:none;stroke-opacity:1"
                                        d="m 0,0 c -26.58,0 -48.129,21.55 -48.131,48.131 0,26.582 21.548,48.131 48.129,48.131 26.584,0 48.128,-21.549 48.128,-48.131 C 48.126,21.55 26.582,0 0,0 Z" /></g><g
                                    transform="translate(33.3184,42.6973)"
                                    id="g24"><path
                                        id="path26"
                                        style="fill:none;stroke-width:2;stroke-linecap:round;stroke-linejoin:miter;stroke-miterlimit:10;stroke-dasharray:none;stroke-opacity:1"
                                        d="M 0,0 15.813,-13.624 31.623,0" /></g><g
                                    transform="translate(49.1309,29.0732)"
                                    id="g28"><path
                                        id="path30"
                                        style="fill:none;stroke-width:2;stroke-linecap:round;stroke-linejoin:miter;stroke-miterlimit:10;stroke-dasharray:none;stroke-opacity:1"
                                        d="M 0,0 V 40.116" /></g></g></g></g></svg>
        </button>
        <div class="l-home-slider__media">
            <div class="c-svg-mask"></div>
        </div>
    </div>
    <div class="l-home-slider__content">
        <div class="c-intro-text"
             data-slide-id="2"
             data-slide-url="https://i.ytimg.com/vi/m2fHM1RvP9o/maxresdefault.jpg">
            <h2 class="c-intro-text__title">Caregiving Evolved</h2>
            <p class="c-intro-text__article">Description of different services dolor sit amet, consectetur adipisc
                ing elit, sed do ei usmod tempor incididunt ut labore et dolore magna aliqua. Ut iI enim ad minim
                veniam, quis nostrud exercita tion ullamco laboris nisi ut aliquip ex ea commodo consequat. </p>
            <a class="btn btn" href="#">Find a Caregiver</a>
            <a class="btn btn_white" href="#">FAQs</a>
        </div>
    </div>
    <div class="l-home-slider__content">
        <div class="c-intro-text"
             data-slide-id="3"
             data-slide-url="http://images2.fanpop.com/images/photos/7800000/Amazing-Nature-Wallpapers-national-geographic-7896771-1280-960.jpg">
            <h2 class="c-intro-text__title">Become a RespiteEase
                caregiver today</h2>
            <p class="c-intro-text__article">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do ei
                usmod tempor incididunt ut labore et dolore magna aliqua. Ut iI enim ad minim veniam, quis nostrud
                exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. </p>
            <a class="btn btn" href="#">Become a Caregiver</a>
            <a class="btn btn_white" href="#">FAQs</a>
        </div>
    </div>
</div>
